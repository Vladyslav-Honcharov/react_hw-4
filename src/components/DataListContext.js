import React, { createContext, useEffect } from "react";
import { useDispatch } from "react-redux";
import { setDataList } from "../redux/actions";
import axios from "axios";

export const DataListContext = createContext();

const DataListProvider = ({ children }) => {
  const dispatch = useDispatch();

  const getData = async () => {
    try {
      const response = await axios.get("data.json");
      dispatch(setDataList(response.data));
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <DataListContext.Provider value={{}}>{children}</DataListContext.Provider>
  );
};

export default DataListProvider;
