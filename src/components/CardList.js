import "./CardList.scss";
import Card from "./Card";
import React from "react";
import { useSelector } from "react-redux";
import PropTypes from "prop-types";

const CardList = () => {
  const dataList = useSelector((state) => state.dataList);

  return (
    <div className="card-list container">
      {dataList.map((card, index) => (
        <Card
          key={index}
          name={card.name}
          price={card.price}
          url={card.url}
          color={card.color}
          index={index}
        />
      ))}
    </div>
  );
};
CardList.propTypes = {
  dataList: PropTypes.array.isRequired,
};

export default CardList;
