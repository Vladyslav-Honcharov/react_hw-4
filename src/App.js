import React from "react";
import Header from "./components/Header";
import CardList from "./components/CardList";
import DataListProvider from "./components/DataListContext";

const App = () => {
  return (
    <>
      <Header />
      <DataListProvider>
        <CardList />
      </DataListProvider>
    </>
  );
};

export default App;
